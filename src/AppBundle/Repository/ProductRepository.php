<?php
/**
 * Created by PhpStorm.
 * User: hungnguyenv4
 * Date: 7/16/2019
 * Time: 4:55 PM
 */

namespace AppBundle\Repository;


use AppBundle\Entity\Product;
use AppBundle\Repository\Interfaces\IProductRepository;
use AppBundle\Repository\BaseRepository;


class ProductRepository extends BaseRepository implements IProductRepository
{

    public function model()
    {
        // TODO: Implement model() method.
        return Product::class;
    }

    public function findByName($name)
    {
        // TODO: Implement findByName() method.
        return $this->repository->findBy(["name"=>$name]);
    }
}