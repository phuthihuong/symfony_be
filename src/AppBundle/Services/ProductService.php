<?php
/**
 * Created by PhpStorm.
 * User: hungnguyenv4
 * Date: 7/16/2019
 * Time: 4:58 PM
 */

namespace AppBundle\Services;


use AppBundle\Entity\Product;
use AppBundle\Repository\Interfaces\IProductRepository;

class ProductService
{
    private $productRepository;

    public function __construct(IProductRepository $productRepository)
    {
        $this->productRepository = $productRepository;
    }

    public function getProductById($id): Product
    {
        return $this->productRepository->findById($id);
    }

    public function getAllProduct()
    {
        return $this->productRepository->findAll();
    }

    public function insert(Product $product){
        $this->productRepository->insert($product);
    }

    public function update(Product $product){
        $this->productRepository->update($product);
    }

    public function deleteProduct(Product $product)
    {
        $this->productRepository->delete($product);
    }
}