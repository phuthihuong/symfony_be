<?php
/**
 * Created by PhpStorm.
 * User: hungnguyenv4
 * Date: 7/2/2019
 * Time: 2:44 PM
 */

namespace AppBundle;


class Demo
{
    private $test;
    public function __construct(Test $test)
    {
        $this->test=$test;
    }
    public function doSomething()
    {
        return $this->test->doSomething();
    }
}