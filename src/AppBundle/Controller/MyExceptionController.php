<?php
/**
 * Created by PhpStorm.
 * User: HUNG
 * Date: 7/21/2019
 * Time: 11:22 PM
 */

namespace AppBundle\Controller;

use Symfony\Bundle\TwigBundle\Controller\ExceptionController;
use Symfony\Component\Debug\Exception\FlattenException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\DebugLoggerInterface;


class MyExceptionController extends ExceptionController
{
    //can setting by using any controller  and any method inside that controller
     //or you can  extend core ExceptionController however you have to change inside service.yml like below
//    AppBundle\Controller\MyExceptionController:
//    public: true
//    arguments:
//    $debug: '%kernel.debug%'
    public function showAction(Request $request, FlattenException $exception, DebugLoggerInterface $logger = null)
    {
        die("error");

    }
}