<?php

namespace AppBundle\Controller;

use AppBundle\Services\PostService;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
/**
 * set prefix for all router in Product controller
 * @Route("/post")
 */
class PostController extends Controller
{
    protected $postService;
    public function __construct(PostService $postService)
    {
        $this->postService=$postService;
    }

    /**
     * @Route("",name="post-list",methods={"GET"})
     */
    public function index()
    {
        $postList=$this->postService->getAll();
        dump($postList);
        exit();
        //return $this->render("post/index.html.twig");
    }
}
