<?php
/**
 * Created by PhpStorm.
 * User: hungnguyenv4
 * Date: 7/16/2019
 * Time: 4:58 PM
 */

namespace AppBundle\Services;


use AppBundle\Entity\Post;
use AppBundle\Repository\Interfaces\IPostRepository;

class PostService
{
    private $postRepository;

    public function __construct(IPostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function getById($id): Post
    {
        return $this->postRepository->findById($id);
    }

    public function getAll()
    {
        return $this->postRepository->findAll();
    }

    public function insert(Post $post){
        $this->postRepository->insert($post);
    }

    public function update(Post $post){
        $this->postRepository->update($post);
    }

    public function delete(Post $post)
    {
        $this->postRepository->delete($post);
    }
}